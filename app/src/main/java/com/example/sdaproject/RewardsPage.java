package com.example.sdaproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class RewardsPage extends AppCompatActivity {

    private TextView numPoints;
    private Button costa;
    private Button mcd;
    private Button dominoes;
    private static final long Start_Points = 100;
    int availpoints = 100;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rewards_page);

        numPoints = (TextView) findViewById(R.id.points);
        mcd = (Button) findViewById(R.id.button1);
        costa = (Button) findViewById(R.id.button2);
        dominoes = (Button) findViewById(R.id.button3);

        mcd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (availpoints >= 30) {
                    availpoints = availpoints - 30;
                    String pointsLeft = String.format(Locale.getDefault(), "%02d", availpoints);
                    numPoints.setText(pointsLeft);
                    Toast.makeText(RewardsPage.this,"enough available points",Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(RewardsPage.this,"Not enough available points",Toast.LENGTH_SHORT).show();
                }
            }
        });

        costa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (availpoints >= 30) {
                    availpoints = availpoints - 30;
                    String pointsLeft = String.format(Locale.getDefault(), "%02d", availpoints);
                    numPoints.setText(pointsLeft);
                } else {
                    Toast.makeText(RewardsPage.this,"Not enough available points",Toast.LENGTH_SHORT).show();
                }
            }
        });

        dominoes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (availpoints >= 50) {
                    availpoints = availpoints - 50;
                    String pointsLeft = String.format(Locale.getDefault(), "%02d", availpoints);
                    numPoints.setText(pointsLeft);
                }
                else
                    Toast.makeText(RewardsPage.this,"Not enough available points",Toast.LENGTH_SHORT).show();
            }
        });
    }

    }
