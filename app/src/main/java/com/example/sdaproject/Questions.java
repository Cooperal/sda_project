package com.example.sdaproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;

import io.perfmark.Tag;

public class Questions extends AppCompatActivity {
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    EditText Q_enjoy, Q_tough, Q_inform;
    Button Q_submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question); //
        Q_enjoy = findViewById(R.id.Q_enjoy);
        Q_tough = findViewById((R.id.Q_tough));
        Q_inform = findViewById(R.id.Q_inform);
        Q_submit = findViewById(R.id.Q_submit);

        Q_submit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                String enjoy = Q_enjoy.getText().toString().trim();
                String tough = Q_tough.getText().toString().trim();
                String inform = Q_inform.getText().toString().trim();

                if(TextUtils.isEmpty(enjoy)){
                    Q_enjoy.setError("You can't not answer this question"); // could let them anyway
                    return;
                }

                if(TextUtils.isEmpty(tough)){
                    Q_tough.setError("You can't not answer this question"); // could let them anyway
                    return;
                }

                if(TextUtils.isEmpty(inform)){
                    Q_inform.setError("You can't not answer this question"); // could let them anyway
                    return;
                }

                /*
                This is where the review would be sent to firebase storage
                 with their name data from profile
                 */
                Map<String, Object> question = new HashMap<>();
                question.put("enjoy", enjoy);
                question.put("tough", tough);
                question.put("inform", inform);
                db.collection("questions")
                        .add(question)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                Toast.makeText(Questions.this , "review uploaded", Toast.LENGTH_LONG).show();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(Questions.this , "Something went wrong", Toast.LENGTH_LONG).show();
                            }
                        });

                startActivity(new Intent(getApplicationContext(),MainActivity.class));
            }

        });

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.

    }
}