package com.example.sdaproject;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.sdaproject.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;

public class Answers extends AppCompatActivity {
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    LinearLayout ll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.answers_layout); //

        ll = findViewById(R.id.answerll);

        //stole some fb code
        //asynchronously retrieve all documents
        db.collection("questions").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override

                    public void onComplete (@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(Answers.this , "here we go", Toast.LENGTH_LONG).show();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                TextView enjoy = new TextView(Answers.this);
                                TextView tough = new TextView(Answers.this);
                                TextView inform = new TextView(Answers.this);
                                TextView spacer = new TextView(Answers.this);

                                enjoy.setText(document.getString("enjoy"));
                                tough.setText(document.getString("tough"));
                                inform.setText(document.getString("inform"));

                                ll.addView(enjoy);
                                ll.addView(tough);
                                ll.addView(inform);
                                ll.addView(spacer);
                            }
                        }   else {
                            Toast.makeText(Answers.this , "something went wrong", Toast.LENGTH_LONG).show();
                        }

                    }

                });

        }

    }

